# Deoptimized version of homework task

require 'json'
require 'ruby-progressbar'

def parse_user(fields)
  {
    id: fields[1].to_i,
    first_name: fields[2],
    last_name: fields[3],
    age: fields[4].to_i
  }
end

def parse_session(fields)
  {
    user_id: fields[1].to_i,
    session_id: fields[2].to_i,
    browser: fields[3].upcase,
    time: fields[4].to_i,
    date: fields[5]
  }
end

def collect_stats_from_users(report, users, progressbar)
  if progressbar
    users_progress = ProgressBar.create(
      title: 'Users',
      total: users.count,
      format: '%t: %J%% %a <%B> %E',
      throttle: 0.1
    )
  end

  users.each do |user|
    user_key = "#{user[:first_name]} #{user[:last_name]}"
    report[:usersStats][user_key] = yield(user)

    users_progress.increment if progressbar
  end
end

def work(filename = 'data.txt', progressbar: false)
  file_lines = File.read(filename).split("\n")

  users = []
  sessions = []

  if progressbar
    line_progress = ProgressBar.create(
      title: 'Lines',
      total: file_lines.count,
      format: '%t: %J%% %a <%B> %E',
      throttle: 0.1
    )
  end

  file_lines.each do |line|
    cols = line.split(',')
    users << parse_user(cols) if cols[0] == 'user'

    if cols[0] == 'session'
      session = parse_session(cols)
      session_user_id = session[:user_id]
      sessions[session_user_id] ||= []
      sessions[session_user_id] << session
    end

    line_progress.increment if progressbar
  end

  # Отчёт в json
  #   - Сколько всего юзеров +
  #   - Сколько всего уникальных браузеров +
  #   - Сколько всего сессий +
  #   - Перечислить уникальные браузеры в алфавитном порядке через запятую и капсом +
  #
  #   - По каждому пользователю
  #     - сколько всего сессий +
  #     - сколько всего времени +
  #     - самая длинная сессия +
  #     - браузеры через запятую +
  #     - Хоть раз использовал IE? +
  #     - Всегда использовал только Хром? +
  #     - даты сессий в порядке убывания через запятую +

  report = {}

  report[:totalUsers] = users.count

  all_browsers = sessions.flatten
                         .map { |s| s[:browser] }
                         .uniq
                         .sort

  # Подсчёт количества уникальных браузеров
  report[:uniqueBrowsersCount] = all_browsers.count

  report[:totalSessions] = sessions.flatten.count

  report[:allBrowsers] = all_browsers.join(',')

  # Статистика по пользователям
  report[:usersStats] = {}

  # Собираем количество сессий по пользователям
  collect_stats_from_users(report, users, progressbar) do |user|
    user_sessions = sessions[user[:id]]
    user_sessions_times = user_sessions.map { |s| s[:time] }
    user_sessions_browsers = user_sessions.map { |s| s[:browser] }

    {
      sessionsCount: user_sessions.count,
      # Собираем количество времени по пользователям
      totalTime: "#{user_sessions_times.sum} min.",
      # Выбираем самую длинную сессию пользователя
      longestSession: "#{user_sessions_times.max} min.",
      # Браузеры пользователя через запятую
      browsers: user_sessions_browsers.sort.join(', '),
      # Хоть раз использовал IE?
      usedIE: user_sessions_browsers.any? { |b| b.start_with? 'INTERNET EXPLORER' },
      # Всегда использовал только Chrome?
      alwaysUsedChrome: user_sessions_browsers.all? { |b| b.start_with? 'CHROME' },
      # Даты сессий через запятую в обратном порядке в формате iso8601
      dates: user_sessions.map { |s| s[:date] }.sort.reverse
    }
  end

  File.write('result.json', "#{report.to_json}\n")
end
