module TestDataHelper
  LARGE_FILE_NAME = 'data_large.txt'

  def self.sub_lines(lines)
    file_name = "test_data/sub_#{lines}.txt"
    generate(lines, file_name) unless File.exist? file_name

    file_name
  end

  def self.generate(lines, file_name)
    `head -n #{lines} #{LARGE_FILE_NAME} > #{file_name}`
  end
end
