describe '#work complexity' do
  let(:sizes) { bench_range(64, 10_000) }

  before do
    sizes.each do |size|
      out_file_name = "test_data/sub_#{size}.txt"
      TestDataHelper.generate(size, out_file_name) unless File.exist?(out_file_name)
    end
  end

  it 'is has a linear complexity' do
    expect { |n, _i| work("test_data/sub_#{n}.txt") }.to perform_linear.in_range(sizes).threshold(0.9)
  end
end
