describe '#work execute time regression' do
  let(:previous_measurement) { 0.0067 }

  it 'is not worse than before' do
    expect { work(TestDataHelper.sub_lines(1_000)) }.to perform_under(previous_measurement).sec.sample(20).times.warmup(5).times
  end
end
