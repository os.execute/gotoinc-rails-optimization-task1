require './task-1'
require './helpers/test_data_helper'
require 'benchmark-perf'

puts Benchmark::Perf.cpu(repeat: 20, warmup: 5) { work(TestDataHelper.sub_lines(1_000)) }
