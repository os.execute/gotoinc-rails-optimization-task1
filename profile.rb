require './task-1'
require 'ruby-prof'
require 'stackprof'
require './helpers/test_data_helper'

GC.disable

def run_profilers
  # Stack-prof
  output = './profile/stackprof.dump'

  StackProf.run(mode: :wall, out: output, interval: 10, raw: true) do
    yield
  end

  `stackprof --d3-flamegraph #{output} > #{output}.html`

  # Ruby-prof
  profile = RubyProf.profile do
    yield
  end
  
  printer = RubyProf::MultiPrinter.new(profile, %I[flat graph_html stack])
  printer.print(path: './profile', profile: 'work')
end

run_profilers {
  work(TestDataHelper.sub_lines(100000))
}
